
local barName, barWidth, barHeight
local anchorX, anchorY

VerticalMorale = {}
VerticalMorale.side = "left"

function VerticalMorale.Initialize()

	-- yeah, your 'hack' breaks my addon, thanks a bunch Fraktyl! :P
	-- find a better fix please. I know there are certain quirks
	-- that need creative, seemingly useless fixes (I'm doing the same in this
	-- very mod), but yours obviously causes it's own problems, or in this case, my problems
	local mods = ModulesGetData();
	for k,v in ipairs(mods) do
		if v.name == "WindowCleaner" then
			if v.isEnabled and not v.isLoaded then
				-- WTB optional dependencies
				ModuleInitialize(v.name)
			end
			break
		end
	end
	-- and why PLAYER_ZONE_CHANGED? wasn't LOADING_END enough?
	if WindowCleaner and WindowCleaner.ResetWindows and type(WindowCleaner.ResetWindows) == "function"
		and WindowCleaner.Windows and type(WindowCleaner.Windows) == "table" then
		-- that should be enough to make sure this hook won't ever cause errors
		local hookWCRW = WindowCleaner.ResetWindows
		WindowCleaner.ResetWindows = function(...)
			local count, reset
			for i = 1, #WindowCleaner.Windows do
				if WindowCleaner.Windows[i].WindowName == "EA_MoraleBar" then
					count = i
					reset = WindowCleaner.Windows[i].ResetWindow
					WindowCleaner.Windows[i].ResetWindow = false
				end
			end
			local ret = hookWCRW(...)
			-- this is the nicest hook I've ever written
			if count then
				WindowCleaner.Windows[count].ResetWindow = reset
			end
			return ret
		end
	end
	
	MoraleButton.SetAnchor = VerticalMorale.MoraleButtonSetAnchor
	MoraleSlottedIndicator.SetAnchor = VerticalMorale.MoraleDotSetAnchor
	
	local hookC = MoraleBar.Create
	MoraleBar.Create = function(self, windowName)
		local ret = hookC(self, windowName)
		
		local x, y = WindowGetDimensions(windowName)
		if x > y then
			-- swap x and y
			x = x + y
			y = x - y
			x = x - y
			WindowSetDimensions(windowName, x, y)
		end
		WindowSetShowing(windowName.."ContentsStatus", false)
		WindowSetShowing(windowName.."ContentsBackground", false)
		WindowSetShowing(windowName.."ContentsOverlay", false)
		--WindowClearAnchors(windowName.."Background")
		--WindowClearAnchors(windowName.."Overlay")
		CreateWindowFromTemplate(windowName.."VerticalBackground", "VerticalMoraleBarBackground", windowName)

		barName = windowName.."VerticalBar"
		CreateWindowFromTemplate(barName, "VerticalMoraleBar", windowName)
		local by, bx = WindowGetDimensions(windowName.."ContentsStatus")
		WindowSetDimensions(barName, bx, by)
		barWidth, barHeight = bx, by
		local _,_,_,ax,ay = WindowGetAnchor(windowName.."ContentsStatus", 1)
		anchorX, anchorY = ax, ay
		WindowClearAnchors(windowName.."ContentsStatus")

		-- do this after the bar so it's on top of it
		CreateWindowFromTemplate(windowName.."VerticalOverlay", "VerticalMoraleBarOverlay", windowName)

		if VerticalMorale.side == "right" then
			WindowAddAnchor(barName, "bottomright", windowName, "bottomright", ay-1, -ax)
			WindowAddAnchor(barName.."1", "bottomright", barName, "bottomright", 0, 0)
			WindowAddAnchor(barName.."2", "bottomright", barName, "bottomright", 0, -by/2)
			WindowAddAnchor(barName.."3", "bottomright", barName, "bottomright", 0, -by*3/4)
	
			WindowAddAnchor(windowName.."VerticalBackground", "bottomright", windowName, "bottomright", y/2, -y/2)
			WindowAddAnchor(windowName.."VerticalOverlay", "bottomright", windowName, "bottomright", y/2, -y/2)
			DynamicImageSetRotation(windowName.."VerticalBackground", 270)
			DynamicImageSetRotation(windowName.."VerticalOverlay", 270)
		else
			WindowAddAnchor(barName, "bottomleft", windowName, "bottomleft", -ay-1, -ax)
			WindowAddAnchor(barName.."1", "bottomleft", barName, "bottomleft", 0, 0)
			WindowAddAnchor(barName.."2", "bottomleft", barName, "bottomleft", 0, -by/2)
			WindowAddAnchor(barName.."3", "bottomleft", barName, "bottomleft", 0, -by*3/4)

			WindowAddAnchor(windowName.."VerticalBackground", "bottomleft", windowName, "bottomleft", -y/2, -y/2)
			WindowAddAnchor(windowName.."VerticalOverlay", "bottomleft", windowName, "bottomleft", -y/2, -y/2)
			DynamicImageSetRotation(windowName.."VerticalBackground", 90)
			DynamicImageSetRotation(windowName.."VerticalOverlay", 90)
			DynamicImageSetTextureOrientation(windowName.."VerticalBackground", true)
			DynamicImageSetTextureOrientation(windowName.."VerticalOverlay", true)
		end
		return ret
	end
	
	local hookSGM = MoraleBar.ShowGainedMorale
	MoraleBar.ShowGainedMorale = function(self, show)
		local ret = hookSGM(self, show)
		if show == false then
			VerticalMorale.SetBarValue(0)
		end
		return ret
	end
	
	local hookSM = MoraleBar.SetMorale
	MoraleBar.SetMorale = function(self, moralePercent, moraleLevel)
		local ret = hookSM(self, moralePercent, moraleLevel)
		if self.m_ShowGainedMorale then
			local fillAmount = StatusBarGetCurrentValue(self.m_StatusBar:GetName())
			VerticalMorale.SetBarValue(fillAmount)
		end
		return ret
	end
	
	if LibSlash then
		LibSlash.RegisterSlashCmd("verticalmorale", function(args)
			if args:match("^side (.+)$") then
				local n = args:match("^side (.+)$")
				if n == "left" or n == "right" then
					VerticalMorale.side = n
					EA_ChatWindow.Print(L"[VerticalMorale]: You must reload the UI for this change to take effect")
				end
			else
				EA_ChatWindow.Print(L"[VerticalMorale]: Valid options are:\n side [left|right]: set the side at which the bar should appear")
			end
		end)
	end
	d("<! Vertical Morale initialized. If you have LibSlash use /vmr to configure.")
end

function VerticalMorale.Shutdown()
  if DoesWindowExist( "EA_MoraleBar" ) then
    local ax, ay = WindowGetDimensions("EA_MoraleBar")
		if ay > ax then
      WindowSetDimensions("EA_MoraleBar", ay, ax)
    end
  end
	if DoesWindowExist("EA_MoraleBarContents") then
		local x, y = WindowGetDimensions("EA_MoraleBarContents")
		if y > x then
			WindowSetDimensions("EA_MoraleBarContents", y, x)
		end
		if not WindowGetShowing("EA_MoraleBarContentsStatus") then
			WindowAddAnchor("EA_MoraleBarContentsStatus", "bottomleft", "EA_MoraleBar", "bottomleft", anchorX, anchorY)
			WindowSetShowing("EA_MoraleBarContentsStatus", true)
		end
		WindowSetShowing("EA_MoraleBarContentsBackground", true)
		WindowSetShowing("EA_MoraleBarContentsOverlay", true)
	end
	if DoesWindowExist(barName) then
		DestroyWindow(barName)
		DestroyWindow("EA_MoraleBarVerticalBackground")
		DestroyWindow("EA_MoraleBarVerticalOverlay")
	end
end

function VerticalMorale.SetBarValue(value)
	if value >= 75 then
		value = value-75
		WindowSetDimensions(barName.."1", barWidth/3, barHeight/2)
		WindowSetDimensions(barName.."2", barWidth*2/3, barHeight/4)
		WindowSetDimensions(barName.."3", barWidth, barHeight*value/100)
	elseif value >= 50 then
		value = value-50
		WindowSetDimensions(barName.."1", barWidth/3, barHeight/2)
		WindowSetDimensions(barName.."2", barWidth*2/3, barHeight*value/100)
		WindowSetDimensions(barName.."3", barWidth, 0)
	else
		WindowSetDimensions(barName.."1", barWidth/3, barHeight*value/100)
		WindowSetDimensions(barName.."2", barWidth*2/3, 0)
		WindowSetDimensions(barName.."3", barWidth, 0)
	end
end

function VerticalMorale.MoraleButtonSetAnchor(self, anchor)
	if VerticalMorale.side == "right" then
		anchor.Point = "bottomright"
		anchor.RelativePoint = "bottomright"
		local x = anchor.XOffset
		anchor.XOffset = anchor.YOffset
		anchor.YOffset = -x
	else
		local x = anchor.XOffset
		anchor.XOffset = -anchor.YOffset
		anchor.YOffset = -x
	end
	Frame.SetAnchor(self, anchor)
end

function VerticalMorale.MoraleDotSetAnchor(self, anchor)
	if VerticalMorale.side == "right" then
		anchor.Point = "right"
		anchor.RelativePoint = "right"
		local x = anchor.XOffset
		anchor.XOffset = anchor.YOffset
		anchor.YOffset = -x
	else
		anchor.Point = "left"
		anchor.RelativePoint = "left"
		local x = anchor.XOffset
		anchor.XOffset = -anchor.YOffset
		anchor.YOffset = -x
	end
	Frame.SetAnchor(self, anchor)
end
